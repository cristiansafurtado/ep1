#ifndef MAPA_HPP
#define MAPA_HPP

#include <string>
#include <vector>
#include <fstream>

#include "embarcacao.hpp"
#include "canoa.hpp"
#include "submarino.hpp"
#include "paviao.hpp"

using namespace std;

class Mapa{
    private:
    vector<vector<Embarcacao*>> frota;
    vector<vector<string>> dados_mapa;
    int dano_geral;

    public:
    Mapa();
    Mapa(string arquivo_mapa);
    vector<vector<Embarcacao*>> get_frota();
    int get_dano_geral();
    void set_dano_geral(int dano_geral);
    vector<vector<string>> get_dados_mapa();
    void coloca_frota();
    void coloca_frota_por_dados_mapa(int tabuleiro [13][13]);
    int acessar_posicao_por_dados(int x, int y, int tabuleiro[13][13]);
    Embarcacao* acessar_posicao(int x, int y);
    void soltar_bomba(int tabuleiro[13][13], Mapa inimigo);
    void desenha_mapa(int tabuleiro[13][13]);

};

#endif