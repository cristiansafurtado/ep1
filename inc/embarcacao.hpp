#ifndef EMBARCACAO_HPP
#define EMBARCACAO_HPP

#include <string>

using namespace std;

class Embarcacao{
    private:
        int tamanho;
        int danos;
        string estado;

    public:
	    Embarcacao();
        int get_tamanho();
        void set_tamanho(int tamanho);
        //virtual void powerUp() = 0;
        string get_estado();
        void set_estado(string estado);
        int get_danos();
        void set_danos(int danos);

};

#endif
