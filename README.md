# EP1 - OO 2019.1 (UnB - Gama)



# Descrição:

&nbsp;&nbsp;    Este projeto consiste em criar um jogo em C++ similar ao game Battleship (ou batalha naval). Através de uma classe com seu construtor sobrecarregado, 
    as posições das embarcações já são pré-definidas destas vindo qualquer arquivo '.txt' que são carregados para dentro do jogo. Dependendo de qual
    argumento este construtor recebe (diretório do mapa), ele irá montar um mapa diferente.


## Observações

* Somente as embarcações do tipo Canoa estão sendo inseridas corretamente dentro da matriz de ponteiros Embarcacao*
 
* Na impressão visual das frotas de cada jogador estão expostas onde ficam as embarcações de cada um, mesmo dentro de condições para evitá-los
 
* Um Mapa é um jogador
 
* Dentro de cada jogador(ou Mapa) existe uma varíavel que indica se todas as embarcações foram derrubadas. Esta não está decrescendo conforme uma bomba atinge 
 a posição de uma embarcação, fazendo com que o frame se repita infinitamente.

* Possui dois tipos de funções que montam a frota de cada jogador: pela atribuição de posições através da matriz de ponteiros do tipo Embarcacao e pelos prórpios
dados que são obtidos do arquivo, que ficam dentro de uma matriz de string.