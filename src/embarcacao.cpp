#include <iostream>
#include <string>
#include "../inc/embarcacao.hpp"

using namespace std;

Embarcacao :: Embarcacao(){
    int tamanho = 0;
    string lado = "";
    int danos = 0;
    string estado = "Navegando";
}

int Embarcacao :: get_tamanho(){
    return tamanho;
}
void Embarcacao :: set_tamanho(int tamanho){
    this->tamanho = tamanho;
}
string Embarcacao :: get_estado(){
    return estado;
}
void Embarcacao :: set_estado(string estado){
    this->estado = estado;
}
int Embarcacao :: get_danos(){
    return danos;
}
void Embarcacao :: set_danos(int danos){
    this->danos = danos;
}