#include <iostream>
#include <string>
#include <fstream>

#include "../inc/embarcacao.hpp"
#include "../inc/submarino.hpp"
#include "../inc/canoa.hpp"
#include "../inc/paviao.hpp"
#include "../inc/mapa.hpp"

using namespace std;

int main(){
    
    int tabuleiro1[13][13];
    int tabuleiro2[13][13];

    Mapa jogador1("doc/map_1.txt");
    jogador1.coloca_frota_por_dados_mapa(tabuleiro1);
    cout << endl;
    Mapa jogador2("doc/map_2.txt");
    jogador2.coloca_frota_por_dados_mapa(tabuleiro2);

        while(jogador1.get_dano_geral()!=0 || jogador2.get_dano_geral()!=0){
            cout << "Vez do jogador 1:" << endl;
            jogador1.desenha_mapa(tabuleiro2);
            jogador1.soltar_bomba(tabuleiro2,jogador2);
            cout << "Vida restante: " << jogador2.get_dano_geral() << "\n" << endl;
            
            cout << "Vez do jogador 2:" << endl;
            jogador2.desenha_mapa(tabuleiro1);
            jogador2.soltar_bomba(tabuleiro1,jogador1);
            cout << "Vida restante: " << jogador1.get_dano_geral() << "\n" << endl;
        }
        
        if(jogador1.get_dano_geral()> jogador2.get_dano_geral())
            cout << "Jogador 1 venceu!" << endl;
        else
            cout << "Jogador 2 venceu!" << endl;
        

    
    return 0;
}
