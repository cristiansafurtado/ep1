#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include "../inc/mapa.hpp"

Mapa::Mapa(){
    
    vector <string> linha;
    dano_geral = 6;
    vector <Embarcacao*> subposicoes;

    for(int i = 0; i < 13; i++){
        for(int j = 0; j < 13; j++){
            linha.push_back("");
        } 
        dados_mapa.push_back(linha);
        linha.clear();
    }

    for(int ii = 0; ii < 13; ii++){
        for(int jj = 0; jj < 13; jj++){
            subposicoes.push_back(NULL);
        } 
        frota.push_back(subposicoes);
        subposicoes.clear();
    }    
}

Mapa::Mapa(string arquivo_mapa){ 

    dano_geral = 6;
    vector <Embarcacao*> subposicoes;
    Embarcacao *ptr =  NULL;
    for(int ii = 0; ii < 13; ii++){
        for(int jj = 0; jj < 13; jj++){
            subposicoes.push_back(ptr);
        } 
        frota.push_back(subposicoes);
        subposicoes.clear();
    } 

    ifstream arquivo;
    arquivo.open(arquivo_mapa.c_str());
    string linha;
    vector <string> dadosI;
 
    int posI;
    while(arquivo.good()){
        getline(arquivo,linha);
        if(linha.length()>1){
            if(linha[0]!='#'){
                while ((posI = linha.find(' ')) > -1) {
                    dadosI.push_back(linha.substr(0, posI));
                    linha = linha.substr(posI+1, linha.size() - posI);
                }                       
                dadosI.push_back(linha);
                dados_mapa.push_back(dadosI);
                dadosI.clear();
            }             
        }

    }
}

vector <vector <Embarcacao*>> Mapa::get_frota(){
    return frota;
}

int Mapa:: get_dano_geral(){
    return dano_geral;
}
void Mapa:: set_dano_geral(int dano_geral){
    this->dano_geral = dano_geral;
}

vector<vector<string>> Mapa :: get_dados_mapa(){
    return dados_mapa;
}

void Mapa:: coloca_frota_por_dados_mapa(int tabuleiro [13][13]){
      for(int i=0; i<13; i++){
          for(int j=0; j<13; j++){
              tabuleiro[i][j] = 0;
          }
      }
      
      
      for(int i=0; i<dados_mapa.size(); i++){
                
                int X = stoi(dados_mapa[i][0]);
                int Y = stoi(dados_mapa[i][1]);

                if (dados_mapa[i][2]=="c"){
                    Canoa *canoaa = new Canoa();
                    tabuleiro[X][Y] = 1;
                }
               
                if (dados_mapa[i][2]=="p") {
                    PAviao *paviaoo = new PAviao();
                    if(dados_mapa[i][3]=="c"){
                            tabuleiro[i][Y] = 1; 
                            tabuleiro[i-1][Y] = 1;
                            tabuleiro[i-2][Y] = 1;
                            tabuleiro[i-3][Y] = 1;
                    }
                    if (dados_mapa[i][3]=="b") {
                            tabuleiro[i][Y] = 1; 
                            tabuleiro[i+1][Y] = 1;
                            tabuleiro[i+2][Y] = 1;
                            tabuleiro[i+3][Y] = 1;
                    }
                    if (dados_mapa[i][3]=="e") {
                            tabuleiro[X][i] = 1;
                            tabuleiro[X][i-1] = 1;
                            tabuleiro[X][i-2] = 1;
                            tabuleiro[X][i-3] = 1;
                    }
                    if (dados_mapa[i][3]=="d") {
                            tabuleiro[X][i] = 1;
                            tabuleiro[X][i+1] = 1;
                            tabuleiro[X][i+2] = 1;
                            tabuleiro[X][i+3] = 1;
                    }
                }
        
                
                if (dados_mapa[i][2]=="s") {
                    Submarino *submarinoo = new Submarino();
                    if(dados_mapa[i][3]=="c"){
                        tabuleiro[i][Y] = 2;
                        tabuleiro[i-1][Y] = 2;
                    }
                    if (dados_mapa[i][3]=="b") {
                        tabuleiro[i][Y] = 2;
                        tabuleiro[i+1][Y] = 2;
                    }
                    if (dados_mapa[i][3]=="e") {
                        tabuleiro[X][i] = 2;
                        tabuleiro[X][i-1] = 2;
                    }
                    if (dados_mapa[i][3]=="d") {
                        tabuleiro[X][i] = 2;
                        tabuleiro[X][i+1] = 2;                        
                    }
                }
        
    }

}



void Mapa :: coloca_frota(){
    
    for(int i=0; i<dados_mapa.size(); i++){
                
            int X = stoi(dados_mapa[i][0]);
            int Y = stoi(dados_mapa[i][1]);

                if (dados_mapa[i][2]=="c"){
                    Canoa *canoaa = new Canoa();
                    frota[X][Y] = canoaa;
                }
               
                if (dados_mapa[i][2]=="p") {
                    
                    PAviao *paviaoo = new PAviao();
                    if(dados_mapa[i][3]=="c"){
                            frota[i][Y] = paviaoo; 
                            frota[i-1][Y] = paviaoo;
                            frota[i-2][Y] = paviaoo;
                            frota[i-3][Y] = paviaoo;
                    }
                    if (dados_mapa[i][3]=="b") {
                            frota[i][Y] = paviaoo; 
                            frota[i+1][Y] = paviaoo;
                            frota[i+2][Y] = paviaoo;
                            frota[i+3][Y] = paviaoo;
                    }
                    if (dados_mapa[i][3]=="e") {
                            frota[X][i] = paviaoo;
                            frota[X][i-1] = paviaoo;
                            frota[X][i-2] = paviaoo;
                            frota[X][i-3] = paviaoo;
                    }
                    if (dados_mapa[i][3]=="d") {
                            frota[X][i] = paviaoo;
                            frota[X][i+1] = paviaoo;
                            frota[X][i+2] = paviaoo;
                            frota[X][i+3] = paviaoo;
                    }
                }
        
                
                if (dados_mapa[i][2]=="s") {
                    Submarino *submarinoo = new Submarino();
                                     
                    if(dados_mapa[i][3]=="c"){
                        frota[i][Y] = submarinoo;
                        frota[i-1][Y] = submarinoo;
                    }
                    if (dados_mapa[i][3]=="b") {
                        frota[i][Y] = submarinoo;
                        frota[i+1][Y] = submarinoo;
                    }
                    if (dados_mapa[i][3]=="e") {
                        frota[X][i] = submarinoo;
                        frota[X][i-1] = submarinoo;
                    }
                    if (dados_mapa[i][3]=="d") {
                        frota[X][i] = submarinoo;
                        frota[X][i+1] = submarinoo;                        
                    }
                }
        
    }
}

Embarcacao* Mapa:: acessar_posicao(int x, int y){
    return frota[x][y];
}

int Mapa :: acessar_posicao_por_dados(int x, int y, int tabuleiro[13][13]){
    return tabuleiro[x][y];
}

void Mapa :: soltar_bomba(int tabuleiro[13][13], Mapa inimigo){
    int x,y;
    
    cin >> x;
    while(x>12 || x<0)  cin >> x;
    cin >> y;
    while(y>12 || y<0)  cin >> y;
    
    if(acessar_posicao_por_dados(x,y,tabuleiro)!=0){
        cout << "Parabéns! Você atingiu uma embarcação!" << endl;
        tabuleiro[x][y]--;
        inimigo.set_dano_geral(inimigo.get_dano_geral()-1);
    }
    else{
        cout << "Nada foi acertado!" << endl;
    }
}
void Mapa :: desenha_mapa(int tabuleiro[13][13]){
    cout << "Mapa do jogador adversário:\n" << endl;
    
    for(int i=0; i<13; i++){
        for(int j=0; j<13; j++){
            if((tabuleiro[i][j]==0 && frota[i][j]==NULL) || (tabuleiro[i][j]!=0 && frota[i][j]!=NULL)){
                cout << "~ ";
            }
            else{
                cout << "* ";
            }
        }
        cout << endl;
    }
}
